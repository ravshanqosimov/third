package uz.pdp.apprestjwt.payload;

import lombok.Data;
import org.springframework.security.core.userdetails.User;

@Data
public class Card {
    private String id;

    private User user;

    private Double balance;


}
